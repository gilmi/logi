{-# LANGUAGE TemplateHaskell #-}

module Language.Logi.Ast where

import Relude hiding (put, get)

import Control.Lens (makeLenses, over, view)
import Data.Data
import Data.Serialize

data Var
  = Var
  { _vVar :: Text
  , _vOrigin :: Text
  }
  deriving (Show, NFData, Data, Typeable, Generic)

instance Eq Var where
  (==) v1 v2 = _vVar v1 == _vVar v2

instance Ord Var where
  (<=) v1 v2 = _vVar v1 <= _vVar v2

newtype Name = Name { _unName :: Text }
  deriving (Show, Eq, Ord, Data, Typeable, Generic, NFData)

newtype Label = Label { _unLabel :: Text }
  deriving (Show, Eq, Ord, Data, Typeable, Generic, NFData)

makeLenses ''Var
makeLenses ''Name
makeLenses ''Label

instance IsString Var where
  fromString = dup Var . fromString

dup :: (a -> a -> b) -> a -> b
dup f x = f x x

instance IsString Name where
  fromString = Name . fromString

data Lit
  = LitInt !Integer
  | LitRational !Rational
  | LitBool !Bool
  | LitString !Text
  | LitSymbol !Text
  deriving (Show, Eq, Ord, Data, Typeable, Generic, NFData)

type Expr = Expr' Var

data NameOrVar
  = N !Name
  | V !Var
  deriving (Show, Eq, Ord, Data, Typeable, Generic, NFData)

data Expr' var
  = ELit !Lit
  | EVar !var
  | ERel !Name
  | ENoV !NameOrVar
  | ERecord (Map Label (Expr' var))
  | EApp !NameOrVar [Expr' var]
  | ELabel (Expr' var) !Label
  deriving (Show, Eq, Ord, Data, Typeable, Generic, NFData, Functor, Foldable, Traversable)

data Fun
  = Fun [Name] !Expr
  deriving (Show, Eq, Ord, Data, Typeable, Generic, NFData)

type AtomOrExpr
  = Either Atom Expr

data Atom
  = Atom
  { _aName :: !Name
  , _aExpr :: !Expr
  }
  deriving (Show, Eq, Ord, Data, Typeable, Generic, NFData)

data Rule
  = Rule
  { _ruleHead :: !Atom
  , _ruleBody :: [AtomOrExpr]
  }
  deriving (Show, Eq, Ord, Data, Typeable, Generic, NFData)

data Statement
  = RuleStmt !Rule
  | Query !Atom
  | Expr !Expr
  | FunDef Name Fun
  | ExternalDef Name FilePath
  deriving (Show, Eq, Ord, Data, Typeable, Generic, NFData)

data Knowledge 
  = Knowledge
  { _functions :: Map Name Fun
  , _externals :: Map Name FilePath
  , _rules :: Map Name (Set Rule)
  , _varSeed :: Integer
  }
  deriving (Show, Eq, Ord, Data, Typeable, Generic, NFData)

instance Serialize Text where
  put = put @ByteString . encodeUtf8
  get = decodeUtf8 @Text @ByteString <$> get

instance Serialize Label
instance Serialize NameOrVar
instance Serialize Name
instance Serialize Var
instance Serialize Lit
instance (Serialize v, Ord v) => Serialize (Expr' v)
instance Serialize Fun
instance Serialize Atom
instance Serialize Rule
instance Serialize Knowledge

makeLenses ''Atom
makeLenses ''Rule
makeLenses ''Knowledge



data SortedRules
  = SortedRules
  { _srAtoms :: Set Atom
  , _srComplex :: Set Rule
  , _srRecursive :: Set Rule
  }
  deriving (Show, Eq, Ord, Data, Typeable, Generic, NFData)

makeLenses ''SortedRules

instance Semigroup SortedRules where
  (<>) sr1 sr2 =
    sr1
      & over srAtoms (<> view srAtoms sr2)
      & over srComplex (<> view srComplex sr2)
      & over srRecursive (<> view srRecursive sr2)

instance Monoid SortedRules where
  mempty = SortedRules mempty mempty mempty

data Result
  = Results (Set Atom)
  | ExprResult !Expr
  | Ok
  | No
  deriving (Show, Eq, Ord, Data, Typeable, Generic, NFData)

type Substitution = Map Var Expr

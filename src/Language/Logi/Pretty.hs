
module Language.Logi.Pretty
  ( module Language.Logi.Pretty
  , pretty
  )
where

import Relude hiding (group, trace)
import Debug.Trace

import Data.Text.Prettyprint.Doc
import Data.Text.Prettyprint.Doc.Render.Text
import qualified Data.Set as S
import qualified Data.Map as M

import Language.Logi.Ast

pptrace :: Pretty a => Text -> a -> a
pptrace txt x = trace (toString $ txt <> ": " <> pp x) x
{-# WARNING pptrace "'pptrace' remains in code" #-}

pp :: Pretty a => a -> Text
pp = ppToText . pretty

ppToText :: Doc ann -> Text
ppToText =
  renderStrict . layoutSmart LayoutOptions{layoutPageWidth = AvailablePerLine 120 1}

ppVar :: Var -> Doc ann
ppVar var = pretty ("$" <> _vOrigin var)

ppName :: Name -> Doc ann
ppName (Name n) = pretty n

ppNameOrVar :: NameOrVar -> Doc ann
ppNameOrVar = \case
  N n -> pretty n
  V v -> pretty v

ppLabel :: Label -> Doc ann
ppLabel (Label n) = pretty n

ppLit :: Lit -> Doc ann
ppLit = \case
  LitInt l -> pretty l
  LitRational l
    | denominator l == 1 ->
      pretty (numerator l)
    | otherwise ->
      pretty (numerator l) <> "/" <> pretty (denominator l)
  LitBool l -> pretty l
  LitString l -> hcat ["\'", pretty l, "\'"]
  LitSymbol l -> hcat [":", pretty l]

ppExpr :: Expr -> Doc ann
ppExpr = \case
  ELit lit -> ppLit lit
  EVar var -> ppVar var
  ERel rel -> ppName rel
  ENoV nov -> "&" <> pretty nov
  EApp name exprs ->
    hcat ["@", pretty name, tupled (map ppExpr exprs)]
  ELabel expr label ->
    group $ hcat [ppExpr expr, ".", pretty label]
  ERecord (M.toList -> mapping) ->
    group $ align $ encloseSep "{" "}" ", " $ map
      (\(k,v) -> pretty k <+> "=" <+> ppExpr v)
      mapping

ppFun :: Fun -> Doc ann
ppFun (Fun args body) =
  hcat ["\\", tupled (map ppName args) <+> "->" <+> ppExpr body]

ppAtom :: Atom -> Doc ann
ppAtom (Atom name expr) =
    hcat [pretty name, tupled [ppExpr expr]]

ppAtomOrExpr :: AtomOrExpr -> Doc ann
ppAtomOrExpr = either ppAtom ppExpr

ppRule :: Rule -> Doc ann
ppRule (Rule rHead rBody) =
  group $ case rBody of
    [] -> ppAtom rHead <> "."
    _  -> vsep
      [ ppAtom rHead <+> ":="
      , indent 4 $ encloseSep "" "." ", " $ map (group . ppAtomOrExpr) rBody
      ]

ppStmt :: Statement -> Doc ann
ppStmt = \case
  RuleStmt rule -> ppRule rule
  Query atom -> ppAtom atom <> "?"
  Expr expr -> ppExpr expr
  FunDef name (Fun args body) -> vsep
    [ "function" <+> pretty name <+> tupled (map pretty args) <+> "="
    , indent 4 $ pretty body <+> "."
    ]
  ExternalDef name path -> vsep
    [ "external" <+> pretty name <+> "="
    , indent 4 $ hcat ["\'", pretty path, "\'"] <+> "."
    ]

ppResults :: Result -> Doc ann
ppResults = \case
  Results facts -> vcat (map ppAtom $ S.toList facts)
  ExprResult expr -> ppExpr expr
  Ok -> "Ok."
  No -> "No."

instance Pretty Result where
  pretty = ppResults

instance Pretty Statement where
  pretty = ppStmt


instance Pretty Rule where
  pretty = ppRule

instance Pretty Atom where
  pretty = ppAtom

instance Pretty Fun where
  pretty = ppFun

instance Pretty Expr where
  pretty = ppExpr

instance Pretty Lit where
  pretty = ppLit

instance Pretty Var where
  pretty = ppVar

instance Pretty Name where
  pretty = ppName

instance Pretty NameOrVar where
  pretty = ppNameOrVar

instance Pretty Label where
  pretty = ppLabel

instance Pretty a => Pretty (Set a) where
  pretty = list . map pretty . S.toList

instance (Pretty k, Pretty v) => Pretty (Map k v) where
  pretty =
    list . map (\(k,v) -> pretty k <+> "=>" <+> align (pretty v)) . M.toList

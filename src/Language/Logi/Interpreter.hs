{-# language BlockArguments #-}
{-# language TemplateHaskell #-}

module Language.Logi.Interpreter where

import Relude

import Control.Monad.Except (MonadError, throwError, foldM)
import qualified Data.Text as T
import qualified Data.Set as S
import qualified Data.Map as M
import qualified Data.Map.Merge.Strict as M
import Control.Lens hiding ((.=), transformM, transform, mapping)
import qualified Data.Generics.Uniplate.Data as U
import Control.Exception (catch)
import System.Random
import Data.Ratio ((%))
import Text.Regex.TDFA ((=~))

import Language.Logi.Ast
import Language.Logi.Error
import Language.Logi.EDSL

import Language.Logi.Pretty

-----------
-- Types --
-----------

data Ctx
  = Ctx
  { _ctxKnowledge :: Knowledge
  , _ctxRandomSeed :: StdGen
  }

data QueryCtx
  = QueryCtx
  { _ctx :: Ctx
  , _qFacts :: Set Atom
  , _fileReader :: FileReader
  }

type FileReader = Name -> FilePath -> IO (Set Atom)

makeLenses ''Ctx
makeLenses ''QueryCtx

type Eval a
  = StateT Ctx (ExceptT Err IO) a

type QueryEval a
  = StateT QueryCtx (ExceptT Err IO) a

------------
-- Runner --
------------

eval :: FileReader -> Statement -> Eval Result
eval fr = \case
  RuleStmt rule -> do
    insertRule rule
    pure Ok

  FunDef name fun@(Fun args _) -> do
    case (M.lookup name builtins, allUniques args) of
      (Just _, _) ->
        throwError $ FunDefDupArgs name
      (Nothing, False) ->
        throwError $ CannotOverrideBuiltinFun name
      (Nothing, True) -> do
        modify $ over (ctxKnowledge . functions) (M.insert name fun)
        pure Ok

  ExternalDef name file -> do
    modify $ over (ctxKnowledge . externals) (M.insert name file)
    pure Ok

  Expr (EApp (N (Name "clear_db")) []) -> do
    modify $ set ctxKnowledge defaultKnowledge
    pure Ok

  Expr (EApp (N (Name "clear")) [ERel (Name name)]) -> do
    modify $ over (ctxKnowledge . rules) $ M.delete (Name name)
    modify $ over (ctxKnowledge . externals) $ M.delete (Name name)
    modify $ over (ctxKnowledge . functions) $ M.delete (Name name)
    pure Ok

  Expr expr -> do
    -- funcsmap <- M.mapWithKey (\k v -> ENoV $ N k) . view (ctxKnowledge . functions) <$> get
    (expr', _) <- runQuery fr $ U.transformM (evalExpr' mempty) expr
    pure $ ExprResult expr'

  Query fact ->
    runQuery fr (evalQuery fact) >>= \case
      (res, _)
        | null res -> pure No
        | otherwise -> pure $ Results res

runQuery :: FileReader -> QueryEval a -> Eval (a, Set Atom)
runQuery fr m = StateT $ \context ->
  fmap
    (\(res, QueryCtx context' facts _) -> ((res, facts), context'))
    (runStateT m (QueryCtx context mempty fr))

errVars :: MonadError Err m => Var -> m Expr
errVars = throwError . FactStmtWithVars

errLabels :: MonadError Err m => Expr -> Label -> m Expr
errLabels expr lbl = throwError (ExprWithoutLabel expr lbl)

runIO' :: IO a -> QueryEval a
runIO' io = do
  result <- liftIO $ catch
    (Right <$> liftIO io)
    (\(SomeException s) -> pure $ Left (show s))
  case result of
    Left err -> throwError (IOError err)
    Right r -> pure r

insertRule :: Rule -> Eval ()
insertRule rule = do
  isValid rule
  newVars <-
    ( fmap M.fromList
    . traverse (\v -> (v,) <$> newVar (v ^. vOrigin))
    . (S.toList . S.fromList) -- nub
    . U.universeBi
    ) rule
  (rule', _) <- runQuery (const $ pure mempty) $ U.transformBiM (replaceVar newVars) rule
  modify $
    over (ctxKnowledge . rules) $
      M.insertWith
        (<>)
        (rule ^. ruleHead . aName)
        (S.singleton rule')

replaceVar :: Map Var Var -> Var -> QueryEval Var
replaceVar sub =
  U.transformBiM $ \v ->
    case M.lookup v sub of
      Nothing ->
        throwError $ InternalError $ unlines
          [ "newVars failed."
          , "var: " <> show v
          , "sub: " <> show sub
          ]
      Just v' ->
        pure v'


isValid :: Rule -> Eval ()
isValid = \case
  Rule rhead []
    | v:_ <- U.universeBi @Atom @Var rhead ->
      throwError $ FactStmtWithVars v
  _ -> pure ()


-----------
-- Query --
-----------

evalQuery :: Atom -> QueryEval (Set Atom)
evalQuery fact = do
  matchFactWithSubs fact [] =<< getSubs True fact

evalExprs :: [Expr] -> Substitution -> QueryEval Bool
evalExprs exprs sub = do
  results <- traverse (evalExpr' sub) exprs
  pure $ all ((==) (ELit $ LitBool True)) results

matchFactWithSubs :: Atom -> [Expr] -> Set Substitution -> QueryEval (Set Atom)
matchFactWithSubs atom exprs (S.toList -> subs) = do
  subsPassingPreds <- filterM (evalExprs exprs) subs
  let
    subs' = mapMaybe (flip applySub $ atom ^. aExpr) subsPassingPreds
  S.fromList . map (Atom $ atom ^. aName)
    <$> traverse (evalExpr errVars errLabels) subs'

getSubs :: Bool -> Atom -> QueryEval (Set Substitution)
getSubs evalName (Atom name expr) = do
  when evalName $ void $ evalRules name
  fmap
    ( S.fromList
    . mapMaybe (match expr)
    . S.toList
    )
    (fetchAtoms name)

evalRules :: Name -> QueryEval (Set Expr)
evalRules name = do
  void $ checkMutualRecursion name mempty name
  nameRules <- sortRules mempty . S.toList <$> getRules name
  addFiles name
  -- first run all of the simple rules and non recursive rules
  -- Then run all of the recursive rules with the accumulated facts
  -- until there are no new facts to add
  mapM_
    (evalRuleNonRecursive True)
    ( S.union
      (S.map (`Rule` []) (nameRules ^. srAtoms))
      (nameRules ^. srComplex)
    )
  evalRecursiveRules name (nameRules ^. srRecursive)
  fetchAtoms name

evalRuleNonRecursive :: Bool -> Rule -> QueryEval ()
evalRuleNonRecursive evalName = \case
  Rule rhead []
    | (v:_) <- getVars (rhead ^. aExpr) ->
      throwError $ FactStmtWithVars v
    | otherwise -> do
      addFact rhead
  (Rule rhead (partitionEithers -> (rbodyatoms, rbodyexprs))) -> do
    allSubs <- map S.toList <$> traverse (getSubs evalName) rbodyatoms
    let
      subRows = do
        row <- loopJoin allSubs
        maybeToList $ foldM mergeSubs mempty row
    atoms <- matchFactWithSubs rhead rbodyexprs (S.fromList subRows)
    mapM_ addFact atoms

evalRecursiveRules :: Name -> Set Rule -> QueryEval ()
evalRecursiveRules name nameRules = do
  atoms <- fetchAtoms name
  for_ nameRules $ \rule -> do
    evalRuleNonRecursive False rule
  atoms' <- fetchAtoms name
  if length atoms == length atoms'
    then pure ()
    else evalRecursiveRules name nameRules

sortRules :: SortedRules -> [Rule] -> SortedRules
sortRules srules = \case
  [] -> srules
  Rule rhead [] : rest ->
    sortRules (over srAtoms (S.insert rhead) srules) rest
  rule@(Rule rhead (partitionEithers -> (rbodyatoms, _))) : rest ->
    if any ((==) (rhead ^. aName) . _aName) rbodyatoms
      then
        sortRules (over srRecursive (S.insert rule) srules) rest
      else
        sortRules (over srComplex (S.insert rule) srules) rest
      
loopJoin :: [[a]] -> [[a]]
loopJoin = \case
  [] -> []
  [l] -> do
    x <- l
    pure [x]
  l : ls -> do
    x <- l
    xs <- loopJoin ls
    pure (x : xs)

match :: Expr -> Expr -> Maybe Substitution
match expr1 expr2 =
  case (expr1, expr2) of
    (ELit x, ELit y)
      | x == y -> pure mempty

    (EVar var, ELit{}) ->
      pure $ M.singleton var expr2

    (EApp _ args, _)
      | [var] <- concatMap getVars args ->
        match (EVar var) expr2

    (EVar var, EApp _ args)
      | [] <- map getVars args ->
        pure $ M.singleton var expr2

    (EVar var, ERecord rec)
      | [] <- concatMap getVars rec ->
        pure $ M.singleton var expr2

    (ERecord rec1, ERecord rec2) ->
        calcSubsRec rec1 rec2
    -- (ELit x, EVar var) ->
    --   pure $ M.singleton var (ELit x)
    _ ->
      Nothing --  $ MatchFailed expr1 expr2

calcSubsRec :: Map Label Expr -> Map Label Expr -> Maybe Substitution
calcSubsRec rec1 rec2 =
  let
    matched :: Maybe [(Expr, Expr)]
    matched = sequence [ (val,) <$> M.lookup label rec2 | (label, val) <- M.toList rec1 ]
  in
     foldM mergeSubs mempty =<< traverse (uncurry match) =<< matched

mergeSubs :: Substitution -> Substitution -> Maybe Substitution
mergeSubs =
  M.mergeA
    M.preserveMissing
    M.preserveMissing
    (M.zipWithAMatched $ \_ x y -> if x == y then Just x else Nothing)

applySub :: Substitution -> Expr -> Maybe Expr
applySub sub =
  U.transformM $ \case
    EVar v ->
      case M.lookup v sub of
        Nothing ->
          Nothing -- throwError $ NoVarInSub v sub
        Just e ->
          pure e
    e -> pure e

evalExpr
  :: (Var -> QueryEval Expr)
  -> (Expr -> Label -> QueryEval Expr)
  -> Expr
  -> QueryEval Expr
evalExpr handleVars handleBadLabels = U.transformM $ \case
  ELit lit -> pure $ ELit $ evalLit lit
  EVar var -> handleVars var -- throwError FactStmtWithVars
  ERel rel -> pure $ ERel rel
  ELabel (ERecord rec) lbl
    | Just e <- M.lookup lbl rec ->
      pure e
  ELabel expr lbl ->
    handleBadLabels expr lbl
  ERecord rec -> pure $ ERecord rec
  ENoV (N n) -> pure $ ENoV (N n)
  ENoV (V var) ->
    evalExpr handleVars handleBadLabels (EVar var)
  EApp (N fun) args ->
    applyBuiltin handleBadLabels fun args
  EApp (V var) args -> do
    evalExpr handleVars handleBadLabels (EVar var) >>= \case
      ENoV (N fun) ->
        applyBuiltin handleBadLabels fun args
      expr ->
        throwError $ NotFunction expr

evalLit :: Lit -> Lit
evalLit = \case
  LitRational r
    | denominator r == 1 ->
      LitInt (numerator r)
  l -> l

evalExpr' :: Substitution -> Expr -> QueryEval Expr
evalExpr' sub expr =
  evalExpr
    (\var -> maybe (throwError $ ExprWithFreeVar expr sub var) pure $ M.lookup var sub)
    errLabels
    expr



applyBuiltin
  :: (Expr -> Label -> QueryEval Expr)
  -> Name
  -> [Expr]
  -> QueryEval Expr
applyBuiltin handleBadLabels name args =
  case M.lookup name builtins of
    Nothing ->
      applyFun handleBadLabels name args
    Just fun ->
      fun args

applyFun
  :: (Expr -> Label -> QueryEval Expr)
  -> Name
  -> [Expr]
  -> QueryEval Expr
applyFun handleBadLabels name exprs = do
  funcs <- _functions . _ctxKnowledge . _ctx <$> get
  case M.lookup name funcs of
    Nothing ->
      throwError $ NotFunction (ENoV $ N name)

    Just (Fun args body)
      | length args == length exprs -> do
        let
          mapping = M.fromList $ zip (map _unName args) exprs
          handleVars' var@Var{_vOrigin} =
            maybe
              (throwError $ ExprWithFreeVar body (M.mapKeys (dup Var) mapping) var)
              pure
              (M.lookup _vOrigin mapping)
        expr <- flip U.transformM body $ \case
          ERel (Name rel) ->
            maybe
              (throwError $ ExprWithFreeVar body (M.mapKeys (dup Var) mapping) (Var rel rel))
              pure
              (M.lookup rel mapping)
          x -> pure x
        evalExpr handleVars' handleBadLabels expr

      | otherwise -> do
        throwError $ FunTypeMismatch name exprs
getVars :: Expr -> [Var]
getVars = S.toList . S.fromList . U.universeBi

addFact :: Atom -> QueryEval ()
addFact (Atom name expr) = do
-- trace (toString $ "fact added: " <> pp fact) $
  expr' <- evalExpr errVars errLabels expr
  modify $ over qFacts $ S.insert $ Atom name expr'

fetchAtoms :: Name -> QueryEval (Set Expr)
fetchAtoms name = do
  S.map _aExpr . S.filter ((==) name . _aName) . _qFacts <$> get

---

defaultKnowledge :: Knowledge
defaultKnowledge =
  Knowledge mempty mempty mempty 0

newVar :: Text -> Eval Var
newVar origin = do
  vvar <- ("_v"<>) . show . _varSeed . _ctxKnowledge <$> get
  let
    var = Var { _vOrigin = origin, _vVar = vvar }
  modify $ over (ctxKnowledge . varSeed) (+1)
  pure var

builtins :: M.Map Name ([Expr] -> QueryEval Expr)
builtins = M.fromList
  -- booleans --
  [ Name "not" .= \case
    [ELit (LitBool b)] ->
      pure (ELit $ LitBool $ not b)
    args -> err (Name "not") args

  , Name "and" .= \case
    args
      | Just bools <- traverse extractBool args ->
        pure $ bool_ $ all id bools
      | otherwise -> err (Name "and") args

  , Name "or" .= \case
    args
      | Just bools <- traverse extractBool args ->
        pure $ bool_ $ any id bools
      | otherwise -> err (Name "or") args

  , Name "lesser" .= comparer "lesser" (<)
  , Name "greater" .= comparer "greater" (>)
  , Name "lesser_equal" .= comparer "lesser" (<=)
  , Name "greater_equal" .= comparer "greater" (>=)

  , Name "equals" .= \case
    [] ->
      pure (ELit $ LitBool True)
    x:xs ->
      pure $
        bool
          (ELit $ LitBool False)
          (ELit $ LitBool True)
          (all (x==) xs)

  , Name "if" .= \case
    [ELit (LitBool test), tru, fals] ->
      pure $ if test then tru else fals
    args -> err (Name "if") args

  -- strings --
  , Name "count" .= \case
    [ELit (LitString str)] -> do
      pure $ int_ $ fromIntegral $ T.length str

    args -> err (Name "count") args

  , Name "concat" .= \case
    args
      | Just strs <- traverse extractStr args ->
        pure $ str_ $ T.concat strs
      | otherwise -> err (Name "concat") args

  , Name "to_str" .= \case
    [ELit lit] ->
      pure $ str_ (pp lit)
    args -> err (Name "to_str") args

  , Name "matches" .= \case
    [ELit (LitString matcher), ELit (LitString input)] ->
      pure $ bool_ (input =~ matcher)
    args -> err (Name "match") args

  -- numbers --
  , Name "sum" .= \case
    args
      | Just ints <- traverse extractInt args ->
        pure $ int_ $ sum ints
      | Just rats <- traverse extractRat args ->
        pure $ U.transformBi evalLit $ rat_ $ sum rats
      | otherwise -> err (Name "sum") args

  , Name "product" .= \case
    args
      | Just ints <- traverse extractInt args ->
        pure $ int_ $ product ints
      | Just rats <- traverse extractRat args ->
        pure $ U.transformBi evalLit $ rat_ $ product rats
      | otherwise -> err (Name "product") args

  , Name "avg" .= \case
    args
      | length (take 1 args) > 0
      , Just rats <- traverse extractRat args ->
        pure $ U.transformBi evalLit $ rat_ $ sum rats / (fromIntegral (length rats) % 1)

      | otherwise -> err (Name "avg") args

  , Name "random" .= \case
    [ELit (LitInt n), ELit (LitInt m)] -> do
      (rand, gen) <- randomR (n, m) . _ctxRandomSeed . _ctx <$> get
      modify $ set (ctx . ctxRandomSeed) gen
      pure $ int_ rand
    args -> err (Name "random") args

  -- aggregates --
  , Name "asum" .= arithAgg "asum" sum
  , Name "aproduct" .= arithAgg "aproduct" product

  , Name "aavg" .= \case
    [ERel rel] -> do
      results <- toList <$> evalRules rel
      case () of
        _
          | length (take 1 results) > 0
          , Just rats <- traverse extractRat results ->
            pure $ U.transformBi evalLit $ rat_ $ sum rats / (fromIntegral (length rats) % 1)

          | otherwise ->
            err (Name "aavg") results
    args ->
      err (Name "aavg") args

  , Name "acount" .= \case
    [ERel rel] -> do
      results <- toList <$> evalRules rel
      pure $ int_ $ fromIntegral $ length results
    args ->
      err (Name "acount") args

  , Name "afold" .= \case
    [ENoV (N fun), initial, ERel rel] -> do
      results <- toList <$> evalRules rel
      foldM (\a b -> evalExpr' mempty $ EApp (N fun) [a, b]) initial results

    args ->
      err (Name "aavg") args
  ]

  where
    err n as =
      throwError $ FunTypeMismatch n as

extractBool :: Expr -> Maybe Bool
extractBool = \case
  ELit (LitBool n) -> Just n
  _ -> Nothing

extractInt :: Expr -> Maybe Integer
extractInt = \case
  ELit (LitInt n) -> Just n
  _ -> Nothing

extractRat :: Expr -> Maybe Rational
extractRat = \case
  ELit (LitRational n) -> Just n
  ELit (LitInt n) -> Just $ n % 1
  _ -> Nothing

extractStr :: Expr -> Maybe Text
extractStr = \case
  ELit (LitString s) -> Just s
  _ -> Nothing

arithAgg :: Text -> (forall a. Num a => [a] -> a) -> [Expr] -> QueryEval Expr
arithAgg name f = \case
  [ERel rel] -> do
    results <- toList <$> evalRules rel
    case () of
      _
        | Just ints <- traverse extractInt results ->
          pure $ int_ $ f ints

        | Just rats <- traverse extractRat results ->
          pure $ U.transformBi evalLit $ rat_ $ f rats

        | otherwise ->
          throwError $ FunTypeMismatch (Name name) [ERel rel]
  args ->
    throwError $ FunTypeMismatch (Name name) args 

comparer :: Text -> (forall a. Ord a => a -> a -> Bool) -> [Expr] -> QueryEval Expr
comparer name comp = \case
  [ELit (LitBool a), ELit (LitBool b)] ->
    pure (ELit $ LitBool $ comp a b)
  [ELit (LitInt a), ELit (LitInt b)] ->
    pure (ELit $ LitBool $ comp a b)
  [ELit (LitString a), ELit (LitString b)] ->
    pure (ELit $ LitBool $ comp a b)
  args -> throwError $ FunTypeMismatch (Name name) args

getRules :: Name -> QueryEval (Set Rule)
getRules name = do
  fromMaybe mempty . M.lookup name . _rules . _ctxKnowledge . _ctx <$> get

addFiles :: Name -> QueryEval ()
addFiles name = do
  M.lookup name . _externals . _ctxKnowledge . _ctx <$> get >>= \case
    Nothing ->
      pure ()
    Just file -> do
      atoms <- runIO' . (\f -> f name file) . _fileReader =<< get
      mapM_ addFact (S.toList atoms)

checkMutualRecursion :: Name -> Set Name -> Name -> QueryEval (Set Name)
checkMutualRecursion original acc current
  | current `S.member` acc = pure acc
  | otherwise = do
    rules' <- getRules current
    let
      acc' = S.insert current acc
      names = S.fromList $ U.universeBi rules'
    when (original /= current && S.member original names) $
      throwError $ MutualRecursion original current
    foldM (checkMutualRecursion original) acc' names

allUniques :: Ord a => [a] -> Bool
allUniques list = length list == length (S.fromList list)

module Language.Logi.Error where

import Relude
import Text.Megaparsec
import Data.Data
import qualified Data.Text as T
import qualified Data.Map as M

import Language.Logi.Ast
import Language.Logi.Pretty

data Err
  = FactStmtWithVars Var
  | NotFunction Expr
  | CannotOverrideBuiltinFun Name
  | FunDefDupArgs Name
  | FunTypeMismatch Name [Expr]
  | MatchFailed Expr Expr
  | NoVarInSub Var (Map Var Expr)
  | ExprWithFreeVar Expr (Map Var Expr) Var
  | ExprWithoutLabel Expr Label
  | ParseError (ParseErrorBundle Text Void)
  | CsvParseError Text
  | MutualRecursion Name Name
  | IOError Text
  | InternalError Text
  | Timeout
  deriving (Show, Eq, Data, Typeable, Generic, NFData)

ppErr :: Err -> Text
ppErr = \case
  FactStmtWithVars var ->
    "Facts cannot contain variables. " <> pp var
  NotFunction name ->
    "`" <> pp name <> "` is not a function."
  CannotOverrideBuiltinFun name ->
    "The function `" <> pp name <> "` is built-in and cannot be overritten."
  FunDefDupArgs name ->
    "The definition of function `" <> pp name <> "` cannot contain duplicate argument names."
  FunTypeMismatch fun args ->
    unwords
      [ "The function"
      , "`" <> pp fun <> "`"
      , "does not accept these arguments:"
      , T.intercalate ", " (map pp args)
      ]
  MatchFailed x y ->
    "Could not match expression " <> pp x <> " and " <> pp y
  NoVarInSub v sub ->
    "Variable " <> pp v <> " not found in substitution." <> unlines
      [ ""
      , pp sub
      ]
  ExprWithoutLabel rec lbl ->
    "The expression " <> pp rec <> " does not contain the label " <> pp lbl <> "."
  ParseError err ->
    toText $ errorBundlePretty err
  CsvParseError err ->
    "CSV parse error in " <> err
  MutualRecursion a b ->
    unwords
      [ "Mutual recursion is not allowed. Found between:"
      , pp a
      , "and"
      , pp b
      ]
  ExprWithFreeVar expr sub var ->
    unlines
      [ "An expression contains unbound variables."
      , "The expression: \t" <> pp expr
      , "The unbound variable: \t" <> pp var
      , "Bound variables: \t" <> pp (M.keys sub)
      ]
  IOError e ->
    "IO: " <> e
  Timeout ->
    "Operation timed out."
  InternalError e ->
    "Internal: " <> e

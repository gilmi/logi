
module Language.Logi.Run where

import Relude

import Control.Monad.Except

import Language.Logi.Ast
import Language.Logi.Csv
import Language.Logi.Error
import Language.Logi.Parser
import Language.Logi.Interpreter

import System.Random

-- * No IO

run :: FilePath -> Text -> Eval Result
run src txt = do
  stmt <- liftEither $
    either (Left . ParseError) pure (runStmtParser src txt)
  eval (const $ pure mempty) stmt

runs :: FilePath -> Text -> Eval [Result]
runs src txt = do
  stmts <- liftEither $
    either (Left . ParseError) pure (runStmtsParser src txt)
  traverse (eval (const $ pure mempty)) stmts

exec :: StdGen -> Knowledge -> FilePath -> Text -> IO (Either Err (Result, Ctx))
exec stdgen knowledge src =
  runExceptT . flip runStateT (Ctx knowledge stdgen) . run src

execs :: StdGen -> Knowledge -> FilePath -> Text -> IO (Either Err ([Result], Ctx))
execs stdgen knowledge src =
  runExceptT . flip runStateT (Ctx knowledge stdgen) . runs src

-- * IO

runIO :: FileReader -> FilePath -> Text -> Eval Result
runIO fr src txt = do
  stmt <- liftEither $
    either (Left . ParseError) pure (runStmtParser src txt)
  eval fr stmt

runsIO :: FileReader -> FilePath -> Text -> Eval [Result]
runsIO fr src txt = do
  stmts <- liftEither $
    either (Left . ParseError) pure (runStmtsParser src txt)
  traverse (eval fr) stmts

execIO :: FileReader -> StdGen -> Knowledge -> FilePath -> Text -> IO (Either Err (Result, Ctx))
execIO fr stdgen knowledge src =
  runExceptT . flip runStateT (Ctx knowledge stdgen) . runIO fr src

execsIO :: FileReader -> StdGen -> Knowledge -> FilePath -> Text -> IO (Either Err ([Result], Ctx))
execsIO fr stdgen knowledge src =
  runExceptT . flip runStateT (Ctx knowledge stdgen) . runsIO fr src

--

readCsvFiles :: FileReader
readCsvFiles name filepath =
  either (error . ppErr) pure . parseCsvFile name =<< readFileText filepath


{- | Module : Language.Logi.EDSL

Logi EDSL

A haskell library that can be used to construct Logi expressions
in Haskell code.

-}

module Language.Logi.EDSL where

import Relude
import qualified Data.Map as M
import Language.Logi.Ast

---------------------
-- * Structure
--
-- Use these functions to create the structure of your statements.


-- | Create a new fact statement in the database.
--
-- Equivalent to: @\<name\>(\<expr\>).@
fact_ :: Text -> Expr -> Statement
fact_ name expr = rule_ name expr []

-- | Create a new rule statement in the database.
--
-- Equivalent to: @\<name\>(\<expr\>) := [ \<facts\> ].@
rule_ :: Text -> Expr -> [AtomOrExpr] -> Statement
rule_ name expr facts =
  RuleStmt $ Rule (Atom (Name name) expr) facts

-- | Query the database.
--
-- Equivalent to: @\<name>(\<expr\>)?@
query_ :: Text -> Expr -> Statement
query_ name expr = Query $ atom_ name expr

-- | Evaluate an expression statement.
expr_ :: Expr -> Statement
expr_ = Expr

-- | Define a new function.
--
-- Equivalent to: @function \<name>(\<args\>) = \<body\>.@
fun_ :: Text -> [Text] -> Expr -> Statement
fun_ fname args body =
  FunDef (Name fname) (Fun (map Name args) body)

-- | Create an atom in a Rule.
atomR_ :: Text -> Expr -> AtomOrExpr
atomR_ name expr = Left (atom_ name expr)

-- | Create an expression in a Rule.
exprR_ :: Expr -> AtomOrExpr
exprR_ = Right

-- | Create an atom. Can be used for testing.
atom_ :: Text -> Expr -> Atom
atom_ name = Atom (Name name)

---------------------
-- * Expressions

-- Use these functions to create expressions.


-- | A variable.
--
-- Equivalent to: @$\<name\>@
var_ :: Text -> Expr
var_ = EVar . dup Var

-- | A record. Use with: @<.=>@
rec_ :: [(Text, Expr)] -> Expr
rec_ = ERecord . M.fromList . fmap (first Label)

-- | Create a record label.
--
-- Equivalent to: @\<k\> = \<v\>@
(.=) :: k -> v -> (k, v)
(.=) = (,)

-- | Access a record label
--
-- Equivalent to: @\<expr\>.\<label\>@
(#) :: Expr -> Text -> Expr
(#) expr label = ELabel expr (Label label)

-- | A string literal
str_ :: Text -> Expr
str_ = ELit . LitString

-- | An integer literal
int_ :: Integer -> Expr
int_ = ELit . LitInt

-- | A Rational literal
rat_ :: Rational -> Expr
rat_ = ELit . LitRational

-- | A boolean literal
bool_ :: Bool -> Expr
bool_ = ELit . LitBool

-- | A symbol literal (not really supported atm)
sym_ :: Text -> Expr
sym_ = ELit . LitSymbol

---------------
-- * Functions
--
-- Scalar and aggregate functions to be used on expressions or tables.

-- | Function application.
--
-- Use this for user defined functions.
app_ :: Text -> [Expr] -> Expr
app_ = EApp . N . Name

-- | Function application with variable name.
--
-- Use this for evaluating function names as variable in user defined functions.
appVar_ :: Text -> [Expr] -> Expr
appVar_ = EApp . V . dup Var

-- ** Booleans

not_ :: Expr -> Expr
not_ expr = EApp (N $ Name "not") [expr]

and_ :: [Expr] -> Expr
and_ = EApp (N $ Name "and")

or_ :: [Expr] -> Expr
or_ = EApp (N $ Name "or")

equals_ :: [Expr] -> Expr
equals_ = EApp (N $ Name "equals")

lesser_ :: Expr -> Expr -> Expr
lesser_ e1 e2 = EApp (N $ Name "lesser") [e1, e2]

greater_ :: Expr -> Expr -> Expr
greater_ e1 e2 = EApp (N $ Name "greater") [e1, e2]

lesser_equal_ :: Expr -> Expr -> Expr
lesser_equal_ e1 e2 = EApp (N $ Name "lesser_equal") [e1, e2]

greater_equal_ :: Expr -> Expr -> Expr
greater_equal_ e1 e2 = EApp (N $ Name "greater_equal") [e1, e2]

-- ** Strings

concat_ :: [Expr] -> Expr
concat_ = EApp (N $ Name "concat")

count_ :: Expr -> Expr
count_ expr = EApp (N $ Name "count") [expr]

-- ** Numbers

sum_ :: [Expr] -> Expr
sum_ = EApp (N $ Name "sum")

avg_ :: [Expr] -> Expr
avg_ = EApp (N $ Name "avg")

random_ :: Expr
random_ = EApp (N $ Name "random") []

randomR_ :: Integer -> Integer -> Expr
randomR_ low high = EApp (N $ Name "random") [int_ low, int_ high]

-- ** Aggregates

-- | Sum a relation.
asum_ :: Text -> Expr
asum_ rel = EApp (N $ Name "asum") [ERel $ Name rel]

-- | Calculate the average of a relation.
aavg_ :: Text -> Expr
aavg_ rel = EApp (N $ Name "aavg") [ERel $ Name rel]

-- | Number of elements in a relation.
acount_ :: Text -> Expr
acount_ rel = EApp (N $ Name "acount") [ERel $ Name rel]

-- | Fold a function over a relation
afold_ :: Text -> Expr -> Text -> Expr
afold_ fun initial rel =
  EApp (N $ Name "afold")
    [ ENoV (N $ Name fun)
    , initial
    , ERel $ Name rel
    ]

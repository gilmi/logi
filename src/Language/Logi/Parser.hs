module Language.Logi.Parser
  ( ParseErr
  , runStmtParser
  , runStmtsParser
  , parseLit
  , parseExpr
  , parseAtom
  , parseStmt
  , parseStmts
  , lexeme
  )
where

import Relude hiding (some, many)
import qualified Relude.Unsafe as U (read)
import Text.Megaparsec hiding (ErrorItem(..), label)
import qualified Text.Megaparsec.Char as P
import qualified Text.Megaparsec.Char.Lexer as L
import qualified Data.Map as M
import Data.Ratio

import Language.Logi.Ast

type Parser = Parsec Void Text

-----------
-- Lexer --
-----------

reservedWords :: [Text]
reservedWords =
  [ "function"
  , "external"
  ]

spaceConsumer :: Parser ()
spaceConsumer = L.space
  P.space1
  (L.skipLineComment "//")
  (L.skipBlockComment "/*" "*/")

symbol :: Text -> Parser ()
symbol = (() <$) . L.symbol spaceConsumer

lexeme :: Parser a -> Parser a
lexeme = L.lexeme spaceConsumer

string :: Parser Text
string = lexeme . fmap toText $ P.char '\'' *> manyTill L.charLiteral (P.char '\'')

var :: Parser Var
var = lexeme var'

var' :: Parser Var
var' =
  let
    pv = fmap (dup Var . toText) $
      P.char '$' *> many (P.letterChar <|> P.digitChar <|> oneOf ['_','-'])
    check v@(Var nm _)
      | nm `elem` reservedWords =
        fail $ toString nm <> " is a reserved word and cannot be used."
      | otherwise = pure v
  in
    (pv >>= check) <?> "variable"

name :: Parser Name
name = lexeme name'

name' :: Parser Name
name' =
  let
    pn = fmap (Name . toText) $
      (:)
        <$> P.letterChar
        <*> many (P.letterChar <|> P.digitChar <|> oneOf ['_','-'])
    check n@(Name nm)
      | nm `elem` reservedWords =
        fail $ toString nm <> " is a reserved word and cannot be used."
      | otherwise = pure n
  in
    (pn >>= check) <?> "name"

nameOrVar :: Parser NameOrVar
nameOrVar =
  V <$> var' <|> N <$> name'

label :: Parser Label
label = Label . _unName <$> name <?> "label"

rel :: Parser Name
rel =
  let
    pr = lexeme $ fmap (Name . toText) $
      (:)
        <$> (P.char '!' *> P.letterChar)
        <*> many (P.letterChar <|> P.digitChar <|> oneOf ['_','-'])
  in
    pr <?> "relation"



number :: Parser Lit
number =
  let
    p = do
      numer <- some P.digitChar <?> "integer/rational literal"
      choice
        [ do
          void $ P.char '/'
          denom <- some P.digitChar
          pure $ LitRational $ U.read numer % U.read denom

        , pure (LitInt $ U.read numer)
        ]
  in
    lexeme p

dot :: Parser ()
dot = symbol "."

comma :: Parser ()
comma = symbol ","

def :: Parser ()
def = symbol ":="

equals :: Parser ()
equals = symbol "="

query :: Parser ()
query = symbol "?"

colon :: Parser ()
colon = () <$ oneOf [':']

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

-- brackets :: Parser a -> Parser a
-- brackets = between (symbol "[") (symbol "]")

braces :: Parser a -> Parser a
braces = between (symbol "{") (symbol "}")

------------
-- Parser --
------------

type ParseErr = ParseErrorBundle Text Void

runStmtParser :: FilePath -> Text -> Either ParseErr Statement
runStmtParser src txt = runParser (parseStmt <* eof) src txt

runStmtsParser :: FilePath -> Text -> Either ParseErr [Statement]
runStmtsParser src txt = runParser (parseStmts <* eof) src txt

parseStmts :: Parser [Statement]
parseStmts =
  some parseStmt

parseStmt :: Parser Statement
parseStmt = do
  choice
    [ fundef
    , externaldef
    , parseAtomOrExpr >>= \case
        Left atom -> do
          choice
            [ dot $> (RuleStmt $ Rule atom [])
            , def *>
              ( fmap RuleStmt $ Rule atom
                <$> (sepBy1 parseAtomOrExpr comma <* dot)
              )
            , query *> pure (Query atom)
            ]
        Right expr ->
          lookAhead eof $> Expr expr
    ]

fundef :: Parser Statement
fundef = do
  void $ lexeme $ P.string "function"
  fname <- name <?> "function name"
  args <- parens (sepBy name comma)
  equals
  body <- parseExpr
  dot
  pure $ FunDef fname (Fun args body)

externaldef :: Parser Statement
externaldef = do
  void $ lexeme $ P.string "external"
  fname <- name <?> "external name"
  equals
  file <- string <?> "file path"
  dot
  pure $ ExternalDef fname (toString file)


parseAtom :: Parser Atom
parseAtom =
  Atom
    <$> (name <?> "name")
    <*> parens (parseExpr <?> "expression")

parseExpr :: Parser Expr
parseExpr = do
  expr <- choice
    [ ELit <$> parseLit <?> "literal"
    , EVar <$> var <?> "variable, prefixed with $"
    , ERel <$> rel <?> "relation"
    , parseRecord <?> "record"
    , EApp
      <$> lexeme (P.char '@' *> nameOrVar)
      <*> parens (sepBy parseExpr comma)
      <?> "function application"
    , ENoV
      <$> lexeme (P.char '&' *> nameOrVar)
      <?> "function reference"
    ]
  choice
    [ lookAhead (try (dot *> label)) *> do
      dot
      labels <- sepBy1 label (lookAhead (try (dot *> label)) *> dot)
      pure $ foldl' ELabel expr labels
    , pure expr
    ]

parseAtomOrExpr :: Parser AtomOrExpr
parseAtomOrExpr =
  choice
    [ Left <$> parseAtom <?> "atom"
    , Right <$> parseExpr <?> "expression"
    ]

parseLit :: Parser Lit
parseLit =
  choice
    [ LitSymbol <$> (colon *> (_unName <$> name)) <?> "symbol literal"
    , LitString <$> string <?> "string literal"
    , number
    , LitBool <$> boolean <?> "boolean literal"
    ]


boolean :: Parser Bool
boolean =
  choice
    [ symbol "true" $> True
    , symbol "false" $> False
    ]

parseRecord :: Parser Expr
parseRecord = fmap (ERecord . M.fromList) $ braces $
  let
    item = (,) <$> label <*> (equals *> parseExpr)
  in
    sepBy item comma

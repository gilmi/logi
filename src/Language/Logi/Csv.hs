{-# language BlockArguments #-}
{-# language TemplateHaskell #-}

module Language.Logi.Csv where

import Relude

import Text.Megaparsec hiding (ErrorItem(..), label)
import qualified Text.Megaparsec.Char as P
import qualified Text.Megaparsec.Char.Lexer as L
import Control.Monad.Except
import qualified Data.Set as S
import qualified Data.Map as M
import qualified Data.Text as T
import Text.CSV (CSV, parseCSV)

import Language.Logi.Ast
import Language.Logi.Error
import Language.Logi.Parser


parseCsvFile :: MonadError Err m => Name -> Text -> m (Set Atom)
parseCsvFile name@(Name nm) txt = do
  case parseCSV (toString nm) (toString txt) of
    Left err -> throwError $ CsvParseError (show err)
    Right [] -> do
      pure mempty
    Right (header:rows) -> do
      csv <- parseCsv header rows
      pure $ S.fromList $ map (Atom name) csv

parseCsv :: MonadError Err m => [String] -> CSV -> m [Expr]
parseCsv header =
  mapM (pure . ERecord . M.fromList <=< parseRow header)

parseRow :: MonadError Err m => [String] -> [String] -> m [(Label, Expr)]
parseRow header row =
  zipWithM
    ( \fieldname (toText -> field) -> do
      field' <- parseField fieldname field
      pure (Label (toText fieldname), ELit field')
    )
    header
    row

parseField :: MonadError Err m => String -> Text -> m Lit
parseField fieldname field = do
  let
    parser =
      lexeme $ choice
        [ (LitString <$> string)
        , try (parseLit <* eof)
        , pure (LitString $ (T.unwords . T.words) field)
        ]
  case runParser parser fieldname field of
    Left err -> throwError (CsvParseError (show err))
    Right x -> pure x

string = fmap toText $ P.char '\"' *> manyTill L.charLiteral (P.char '\"')

-- I want to get some analytics about visits to my website using my proglang

test :: IO ()
test = do
  v <- readFileText "/home/suppi/visits.csv" 
  let p = runExcept $ parseCsvFile (Name "visits") visits
      visits = T.unlines $ take 10 $ T.lines v
  case p of
    Left err -> putTextLn $ ppErr err
    Right x -> putTextLn $ show x

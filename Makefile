.PHONY: setup

setup:
	stack setup

.PHONY: build

build:
	stack build --fast

.PHONY: dev

dev:
	stack test --fast --file-watch

.PHONY: run

run:
	stack build --fast && stack exec logi-repl

.PHONY: test

test:
	stack test --fast

.PHONY: clean_all

clean_all:
	stack clean


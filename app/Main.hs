module Main where

import Relude
import Relude.Extra.Lens
import System.Random
import qualified Data.Text as T
import qualified Data.Map as M
import Control.Exception
import Data.Serialize (encode, decode)
import System.Console.Repline
import System.Timeout
import System.Mem

import Language.Logi

type Repl a = HaskelineT (StateT Ctx IO) a


main :: IO ()
main = repl

repl :: IO ()
repl = do
  seed <- getStdGen
  let
    context = Ctx defaultKnowledge seed

  flip evalStateT context $ evalReplOpts $
    ReplOpts
      { banner = customBanner
      , command = cmd . toText
      , options = fmap (\(a, b) -> (toString a, b . toText)) opts
      , prefix = Just ':'
      , multilineCommand = Nothing
      , tabComplete = Combine
        (Word0 completer)
        (Prefix (wordCompleter byWord) defaultMatcher)
      , initialiser = ini
      , finaliser = final
      }

-- Evaluation : handle each line user inputs
cmd :: Text -> Repl ()
cmd input = do
  rctx@(Ctx knowledge seed) <- get
  
  ctx' <- liftIO $ timeout (3_000_000) $
    execIO readCsvFiles seed knowledge "repl" input >>= \case
      Left err -> do
        putTextLn $ "*** Error: " <> ppErr err
        pure rctx
      Right (results, ctx') -> do
        (putTextLn . ppToText . ppResults) results
        pure ctx'
  maybe (putTextLn "Operation timed out.") put ctx'
  liftIO $ performGC

trim :: Text -> Text
trim = T.dropWhile (==' ') . T.reverse . T.dropWhile (==' ') . T.reverse

-- Commands
help :: [Text] -> Repl ()
help args = liftIO $ print $ "Help: " ++ show args

load :: Text -> Repl ()
load args = do
  let
    action = do
      decode <$> readFileBS (toString $ trim args)
  result <- liftIO $
    catch action $ \(SomeException e) -> pure (Left $ show e)
  case result of
    Right knowledge' -> do
      modify $ set ctxKnowledge knowledge'
      putTextLn "done."
    Left err ->
      putStrLn err

save :: Text -> Repl ()
save args = do
  knowledge <- gets _ctxKnowledge
  let
    action = do
      writeFileBS (toString args) (encode knowledge)
      putTextLn "done."
  liftIO $
    catch action $ \(SomeException e) -> putTextLn (show e)

-- Options
opts :: [(Text, Text -> Repl ())]
opts =
  let
    fullopts =
      [ ("help", help . words) -- :help
      , ("load", load) -- :load
      , ("save", save) -- :load
      , ("quit", const (final *> abort)) -- :load
      ]
  in
    fullopts <> map (first (T.take 1)) fullopts

-- Tab Completion: return a completion for partial words entered
completer :: MonadState Ctx m => WordCompleter m
completer n = do
  names <- gets $ map (toString . _unName) . M.keys . view (ctxKnowledge . rules)
  return $ filter (isPrefixOf n) names

-- Completer
defaultMatcher :: (MonadState Ctx m, MonadIO m) => [(String, CompletionFunc m)]
defaultMatcher =
  -- Commands
  [ (":load", fileCompleter)
  , (":l", fileCompleter)
  , (":save", fileCompleter)
  , (":s", fileCompleter)
  ]

byWord :: Monad m => WordCompleter m
byWord (toText -> n) = do
  let names = fmap ((":" <>) . fst) opts
  return $ fmap toString $ filter (T.isPrefixOf n) names

-- Initialiser function
ini :: Repl ()
ini = liftIO $ putTextLn ">>> Welcome to logi-repl."

-- Finaliser function
final :: Repl ExitDecision
final = do
  liftIO $ putTextLn "Goodbye!"
  return Exit

customBanner :: MultiLine -> Repl String
customBanner = \case
  SingleLine -> pure "> "
  MultiLine -> pure "| "


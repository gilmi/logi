* logi

A silly half-baked implementation of a logic programming language heavily inspired by datalog.

Have fun with it but don't use it for anything important please :)

Might go well with [[https://gitlab.com/gilmi/simple-file-db][simple-file-db]]

** Build

Download [[https://haskellstack.org][Stack]]

#+BEGIN_SRC shell
stack build
#+END_SRC

Run REPL with:

#+BEGIN_SRC shell
stack run
#+END_SRC

** Simple Examples
*** Ancestor

#+BEGIN_SRC haskell
> parent({parent='john', child='william'}).
Ok.
> parent({parent='william', child='ruth'}).
Ok.
> ancestor({ancestor=$X, offspring=$Y}) := parent({parent=$X, child=$Y}).
Ok.
> ancestor({ancestor=$X, offspring=$Z}) := parent({parent=$X, child=$Y}), ancestor({ancestor=$Y, offspring=$Z}).
Ok.
> ancestor($x)?
ancestor({ancestor = 'john', offspring = 'ruth'})
ancestor({ancestor = 'john', offspring = 'william'})
ancestor({ancestor = 'william', offspring = 'ruth'})
#+END_SRC

*** Negative Join

#+BEGIN_SRC haskell
> a(1).
Ok.
> a(2).
Ok.
> b(1).
Ok.
> b(3).
Ok.
> c({a=$a, b=$b}) := a($a), b($b), @not(@equals($a, $b)).
Ok.
> c($x)?
c({a = 1, b = 3})
c({a = 2, b = 1})
c({a = 2, b = 3})
#+END_SRC

*** Iterate
#+BEGIN_SRC haskell
> a(1).
Ok.
> a(@sum($x,1)) := a($x), @lesser($x, 10).
Ok.
> a($x)?
a(1)
a(2)
a(3)
a(4)
a(5)
a(6)
a(7)
a(8)
a(9)
a(10)
#+END_SRC
*** Aggregates
(Carrying on with the ~a~ from before)

#+BEGIN_SRC haskell
> @asum(!a)
55
#+END_SRC

#+BEGIN_SRC haskell
> @aavg(!a)
11/2
#+END_SRC

#+BEGIN_SRC haskell
> @afold(&product, 1, !a)
3628800
#+END_SRC
*** Filter by Label
#+BEGIN_SRC haskell
> product({ name = 'book', price = 17 }).
Ok.
> product({ name = 'shirt', price = 700 }).
Ok.
> expensive($x) := product($x), @greater($x.price, 100).
Ok.
> expensive($x)?
expensive({name = 'shirt', price = 700})
#+END_SRC
*** User Defined Functions
#+BEGIN_SRC haskell
> function between(low,n,high) = @and(@lesser($low,$n), @greater($high, $n)).
Ok.
> a(1).
Ok.
> a(@sum($x,1)) := a($x), @lesser($x, 10).
Ok.
> b($x) := a($x), @between(3, $x, 7).
Ok.
> b($x)?
b(4)
b(5)
b(6)
#+END_SRC

#+BEGIN_SRC haskell
> function app(f,n) = @$f($n,1,2).
Ok.
> @app(&sum, 10)
13
> @app(&product, 10)
20
#+END_SRC
** EDSL

Logi can also be used as an EDSL.

Check out the [[src/Language/Logi/EDSL.hs][EDSL module]] for documentation and the [[test/EDSL.hs][EDSL test module]] for examples.

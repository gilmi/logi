
module Query where

import Relude
import Test.Hspec

import Language.Logi

import Utils

tests :: Spec
tests = do
  describe "Interpret" $ do
    it "parent({c='john', p='douglas'})." $ do
      shouldBe
        ( runE'
          [ "parent({c='john', p='douglas'})."
          , "parent($x)?"
          ]
        )
        (resE [atom_ "parent" (rec_ ["p" .= str_ "douglas", "c" .= str_ "john"])])

    it "partial record match" $ do
      shouldBe
        ( runE'
          [ "weapons({name='dagger', die='d4'})."
          , "weapons({name='shortsword', die='d6'})."
          , "weapons({name=$X, die='d4'})?"
          ]
        )
        (resE [atom_ "weapons" (rec_ ["name" .= str_ "dagger", "die" .= str_ "d4"])])

    it "join weapons class" $ do
      shouldBe
        ( runE'
          [ "weapon({name='dagger', die='d4'})."
          , "weapon({name='shortsword', die='d6'})."
          , "class({name='wizard', hit_die='d6'})."
          , "result({class=$X, weapon=$Y}) := weapon({name=$Y, die=$D}), class({name=$X, hit_die=$D})."
          , "result($x)?"
          ]
        )
        (resE [atom_ "result" (rec_ ["class" .= str_ "wizard", "weapon" .= str_ "shortsword"])])

    it "ancestor" $ do
      shouldBe
        ( runE'
          [ "parent({parent='john', child='william'})."
          , "parent({parent='william', child='ruth'})."
          , "ancestor({ancestor=$X, offspring=$Y}) := parent({parent=$X, child=$Y})."
          , "ancestor({ancestor=$X, offspring=$Z}) := parent({parent=$X, child=$Y}), ancestor({ancestor=$Y, offspring=$Z})."
          , "ancestor($x)?"
          ]
        )
        ( resE
          [ atom_ "ancestor" $ rec_ ["ancestor" .= str_ "john", "offspring" .= str_ "william"]
          , atom_ "ancestor" $ rec_ ["ancestor" .= str_ "william", "offspring" .= str_ "ruth"]
          , atom_ "ancestor" $ rec_ ["ancestor" .= str_ "john", "offspring" .= str_ "ruth"]
          ]
        )

    it "access label" $ do
      shouldBe
        ( runE'
          [ "{ a = 1 }.a"
          ]
        )
        ( resExpr $ int_ 1
        )

    it "filter by label" $ do
      shouldBe
        ( runE'
          [ "product({ name = 'book', price = 17 })."
          , "product({ name = 'shirt', price = 700 })."
          , "expensive($x) := product($x), @greater($x.price, 100)."
          , "expensive($x)?"
          ]
        )
        ( resE
          [ atom_ "expensive" $ rec_ ["name" .= str_ "shirt", "price" .= int_ 700 ]
          ]
        )


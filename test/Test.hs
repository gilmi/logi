{-# LANGUAGE NumericUnderscores #-}

import Relude
import Test.Hspec
import System.Timeout

import qualified Query
import qualified Functions
import qualified Aggregates
import qualified EDSL
import qualified Csv

main :: IO ()
main = do
  timeout (20_000_000) tests
    >>= maybe (error "suite timeout") (const $ pure ())

tests :: IO ()
tests =
  hspec $ do
    Query.tests
    Functions.tests
    Aggregates.tests
    EDSL.tests
    Csv.tests

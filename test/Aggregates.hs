module Aggregates where

import Relude
import Test.Hspec
import Data.Ratio ((%))

import Language.Logi

import Utils

tests :: Spec
tests = do
  describe "aggregates" $ do

    it "asum" $ do
      shouldBe
        ( runE'
          [ "a(1)."
          , "a(2)."
          , "a(3)."
          , "@asum(!a)"
          ]
        )
        ( resExpr $ int_ 6
        )

    it "aavg" $ do
      shouldBe
        ( runE'
          [ "a(1)."
          , "a(2)."
          , "a(3)."
          , "@aavg(!a)"
          ]
        )
        ( resExpr $ int_ 2
        )

    it "aavg rational" $ do
      shouldBe
        ( runE'
          [ "a(1/4)."
          , "a(2/4)."
          , "a(3/4)."
          , "@aavg(!a)"
          ]
        )
        ( resExpr $ rat_ (1 % 2)
        )

    it "acount" $ do
      shouldBe
        ( runE'
          [ "a(1)."
          , "a(2)."
          , "a(3)."
          , "@acount(!a)"
          ]
        )
        ( resExpr $ int_ 3
        )

    it "iterate and fold" $ do
      shouldBe
        ( runE'
          [ "a(1)."
          , "a(@sum($x,1)) := a($x), @lesser($x, 10)."
          , "@afold(&product, 1, !a)"
          ]
        )
        ( resExpr (int_ 3628800)
        )

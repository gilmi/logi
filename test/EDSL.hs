module EDSL where

import Relude
import Test.Hspec

import Language.Logi

import Utils

tests :: Spec
tests = do
  describe "edsl" $ do

    it "fact and query asum" $ do
      shouldBe
        ( evalE'
          [ fact_ "a" (int_ 1)
          , fact_ "a" (int_ 2)
          , fact_ "a" (int_ 3)
          , expr_ $ asum_ "a"
          ]
        )
        ( resExpr $ int_ 6
        )

    it "fact and query acount" $ do
      shouldBe
        ( evalE'
          [ fact_ "a" (int_ 1)
          , fact_ "a" (int_ 2)
          , fact_ "a" (int_ 3)
          , expr_ $ acount_ "a"
          ]
        )
        ( resExpr $ int_ 3
        )

    it "negative join" $ do
      shouldBe
        ( evalE'
          [ fact_ "a" (int_ 1)
          , fact_ "a" (int_ 2)
          , fact_ "b" (int_ 2)
          , fact_ "b" (int_ 3)
          , rule_ "c" (rec_ ["a" .= var_ "a", "b" .= var_ "b"])
            [ atomR_ "a" (var_ "a")
            , atomR_ "b" (var_ "b")
            , exprR_ (not_ (equals_ [var_ "a", var_ "b"]))
            ]
          , query_ "c" (var_ "x")
          ]
        )
        ( resE
          [ atom_ "c" $ rec_ ["a" .= int_ 1, "b" .= int_ 2]
          , atom_ "c" $ rec_ ["a" .= int_ 2, "b" .= int_ 3]
          , atom_ "c" $ rec_ ["a" .= int_ 1, "b" .= int_ 3]
          ]
        )

    it "access label" $ do
      shouldBe
        ( evalE'
          [ expr_ $ rec_ ["a" .= int_ 1] # "a"
          ]
        )
        ( resExpr $ int_ 1
        )

    it "udf between" $ do
      shouldBe
        ( evalE'
          [ fact_ "a" (int_ 1)
          , rule_ "a" (sum_ [var_ "x", int_ 1])
            [ atomR_ "a" (var_ "x")
            , exprR_ (lesser_ (var_ "x") (int_ 10))
            ]
          , fun_ "between" ["low", "n", "high"] $
            and_ [lesser_ (var_ "low") (var_ "n"), greater_ (var_ "high") (var_ "n")]
          , rule_ "b" (var_ "x")
            [ atomR_ "a" (var_ "x")
            , exprR_ $ app_ "between" [int_ 3, var_ "x", int_ 7]
            ]
          , query_ "b" (var_ "x")
          ]
        )
        ( resE $ map (atom_ "b" . int_) [4..6]
        )

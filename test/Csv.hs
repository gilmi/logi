module Csv where

import Relude
import Test.Hspec
import qualified Data.Text as T

import Language.Logi

import Utils

tests :: Spec
tests = do
  describe "csv" $ do
    it "parse and count" $
      shouldBe
        ( runEWith' ["file" .= mycsv]
          [ "external file = 'file'."
          , "@acount(!file)"
          ]
        )
        ( resExpr $ int_ 2
        )

mycsv :: Text
mycsv =
  T.intercalate "\n" $ map (T.intercalate ",")
    [ ["a", "b", "c"]
    , ["178","hello world", "true"]
    , ["178/11","'hello world'", "false"]
    ]

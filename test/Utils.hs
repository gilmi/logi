module Utils where

import Relude
import System.IO.Unsafe
import qualified Relude.Unsafe as U

import qualified Data.Set as S
import qualified Data.Map as M
import System.Random

import Language.Logi

evalWith :: [(FilePath, Text)] -> Statement -> Eval Result
evalWith files stmt = eval (myreader $ M.fromList files) stmt

myreader :: Map FilePath Text -> FileReader
myreader files name filepath = do
  txt <- maybe (error "File not found.") pure $ M.lookup filepath files
  either (error . ppErr) pure $ parseCsvFile name txt

myeval :: Statement -> Eval Result
myeval stmt = eval (const $ pure mempty) stmt

evalK :: Statement -> Either Text (Set Rule)
evalK =
  fmap getFacts . runK . myeval

eval' :: Statement -> Either Text Result
eval' =
  runE . myeval

evalE' :: [Statement] -> Either Text Result
evalE' stmts = runE $ U.last <$> traverse myeval stmts

defaultCtx =
  Ctx defaultKnowledge (mkStdGen 7)

runEWith' :: [(FilePath, Text)] -> [Text] -> Either Text Result
runEWith' files txts = runE $ do
  U.last <$> traverse (runIO (myreader $ M.fromList files) "test") txts


runE' :: [Text] -> Either Text Result
runE' txts = runE $ do
  U.last <$> traverse (run "test") txts

runK :: Eval a -> Either Text Ctx
runK = first ppErr . unsafePerformIO . runExceptT . flip execStateT defaultCtx

runE :: Eval a -> Either Text a
runE = first ppErr . unsafePerformIO . runExceptT . flip evalStateT defaultCtx

getFacts :: Ctx -> Set Rule
getFacts =
  S.unions . toList . _rules . _ctxKnowledge

resExpr :: Expr -> Either Text Result
resExpr = pure . ExprResult

resE :: [Atom] -> Either Text Result
resE = pure . Results . S.fromList

resK :: [Rule] -> Either Text (Set Rule)
resK = pure . S.fromList

module Functions where

import Relude
import Test.Hspec
import Data.Ratio ((%))

import Language.Logi

import Utils

tests :: Spec
tests = do
  describe "functions" $ do
    it "concat john douglas" $ do
      shouldBe
        (eval' $ expr_ $ concat_ [ str_ "john" , concat_ [str_ " ", str_ "douglas"]])
        (resExpr (str_ "john douglas"))

    it "add 3 5" $ do
      shouldBe
        (eval' $ Expr $ sum_ [int_ 3, int_ 5])
        (resExpr (int_ 8))

    it "add and query" $ do
      shouldBe
        ( runE $ do
          void $ myeval $ fact_ "merry" $ sum_ [int_ 3, int_ 5]
          void $ myeval $ fact_ "merry" $ sum_ [int_ 3, int_ 6]
          myeval $ query_ "merry" $ var_ "i"
        )
        (resE [atom_ "merry" (int_ 9), atom_ "merry" (int_ 8)])

    it "sum ref and query" $ do
      shouldBe
        ( runE $ do
          void $ myeval $ fact_ "merry" $ sum_ [int_ 3, int_ 5]
          void $ myeval $ rule_ "jerry" (var_ "x")
            [atomR_ "merry" $ var_ "x"]
          myeval $ query_ "jerry" $ var_ "i"
        )
        (resE [atom_ "jerry" (int_ 8)])

    it "@concat('john', ' ', 'douglas')" $ do
      shouldBe
        (runE' ["@concat('john', ' ', 'douglas')"])
        (resExpr (str_ "john douglas"))

    it "@concat('john', concat(' ', 'douglas')" $ do
      shouldBe
        (runE' ["@concat('john', @concat(' ', 'douglas'))"])
        (resExpr (str_ "john douglas"))

    it "sum 3 5" $ do
      shouldBe
        (runE' ["@sum(3,5)"])
        (resExpr (int_ 8))

    it "avg 1,2,3,4,5" $ do
      shouldBe
        (runE' ["@avg(1,2,3,4,5,6)"])
        (resExpr (rat_ (7%2)))

    it "if then" $ do
      shouldBe
        (runE' ["@if(@lesser(3,5), 'hello', 'world')"])
        (resExpr (str_ "hello"))

    it "if else" $ do
      shouldBe
        (runE' ["@if(@greater(3,5), 'hello', 'world')"])
        (resExpr (str_ "world"))

    it "count 'hello world'" $ do
      shouldBe
        (runE' ["@count('hello world')"])
        (resExpr (int_ 11))

    it "sum and query" $ do
      shouldBe
        ( runE'
          [ "merry(@sum(3,5))."
          , "merry(@sum(4,6))."
          , "merry($i)?"
          ]
        )
        (resE [atom_ "merry" (int_ 8), atom_ "merry" (int_ 10)])

    it "sum ref and query" $ do
      shouldBe
        ( runE'
          [ "merry(@sum(3,5))."
          , "jerry($x) := merry($x)."
          , "jerry($i)?"
          ]
        )
        (resE [atom_ "jerry" (int_ 8)])

    it "query function" $ do
      shouldBe
        ( runE'
          [ "result(7)."
          , "result(@sum($x,7))?"
          ]
        )
        (resE [atom_ "result" (int_ 14)])

    it "inner join" $ do
      shouldBe
        ( runE'
          [ "a(1)."
          , "a(2)."
          , "b(1)."
          , "b(3)."
          , "c({a=$a, b=$b}) := a($a), b($b), @equals($a, $b)."
          , "c($x)?"
          ]
        )
        ( resE
          [ atom_ "c" $ rec_ ["a" .= int_ 1, "b" .= int_ 1]
          ]
        )

    it "negative join" $ do
      shouldBe
        ( runE'
          [ "a(1)."
          , "a(2)."
          , "b(1)."
          , "b(3)."
          , "c({a=$a, b=$b}) := a($a), b($b), @not(@equals($a, $b))."
          , "c($x)?"
          ]
        )
        ( resE
          [ atom_ "c" $ rec_ ["a" .= int_ 1, "b" .= int_ 3]
          , atom_ "c" $ rec_ ["a" .= int_ 2, "b" .= int_ 1]
          , atom_ "c" $ rec_ ["a" .= int_ 2, "b" .= int_ 3]
          ]
        )

    it "iterate" $ do
      shouldBe
        ( runE'
          [ "a(1)."
          , "a(@sum($x,1)) := a($x), @lesser($x, 50)."
          , "a($x)?"
          ]
        )
        ( resE $ map (atom_ "a" . int_) [1..50]
        )

    it "udf between" $ do
      shouldBe
        ( runE'
          [ "a(1)."
          , "a(@sum($x,1)) := a($x), @lesser($x, 10)."
          , "function between(low,n,high) = @and(@lesser($low,$n), @greater($high, $n))."
          , "b($x) := a($x), @between(3, $x, 7)."
          , "b($x)?"
          ]
        )
        ( resE $ map (atom_ "b" . int_) [4..6]
        )

    it "udf with functions as input" $ do
      shouldBe
        ( runE'
          [ "a(1)."
          , "a(@sum($x,1)) := a($x), @lesser($x, 10)."
          , "a(@sum($x,10)) := a($x), @equals(@app(&sum, $x), 10)."
          , "function app(f,n) = @$f($n,1,2)."
          , "a($x)?"
          ]
        )
        ( resE $ atom_ "a" (int_ 17) : map (atom_ "a" . int_) [1..10]
        )
